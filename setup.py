from setuptools import setup

setup(name='solidclient',
      version='0.1',
      description='A solid client in python',
      url='https://gitlab.com/arbetsformedlingen/individdata/oak/python_solid_client.git',
      author='Fredrik Höllinger',
      author_email='fredrik.hollinger@arbetsformedlingen.se',
      license='MIT',
      packages=['solidclient'],
      python_requires = '>= 3.6',
      )
