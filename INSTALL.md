### Installing

Enter your virtual environment where you want this module installed and run
`pip3 install .`
in this folder.

### Usage:
```
	from solidclient import solid_client

	# Create a JWK and an access token, these come from the idp
	keypair = jwcrypto.JWK.from_json(key)

	access_token = jwcrypto.JWT

	solid_session = solid_client.SolidClient.login_key_access_token(keypair, access_token)

	response = solid_session.get(uri)

	print(response.status_code)

	print(response.get_graph())

	rdf_graph = response.get_graph()

	for s, o, p in rdf_graph:
		print(s, o, p)

```

